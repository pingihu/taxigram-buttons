//
//  ArcButtonView.swift
//  Buttons
//
//  Created by Ping Inay Hu on 01.03.16.
//  Copyright © 2016 Ping Hu. All rights reserved.
//

import UIKit

@IBDesignable class ArcButtonView: UIButton {
    
    var π = CGFloat(M_PI)
    var previousTouchPoint = CGPoint()
    var previousTouchResponse = false
    var arc = UIBezierPath()
    
    var activate : Bool = false {
        willSet {
            self.setNeedsDisplay()
        }
    }
    
    var arcWidth : CGFloat
    @IBInspectable var startRadians : CGFloat
    @IBInspectable var endRadians : CGFloat
    @IBInspectable var buttonColor: UIColor
    
    // Initializers
    
    required override init(frame: CGRect){
        self.startRadians = 7 / 6
        self.endRadians = 11/6
        self.buttonColor = UIColor.greenColor()
        self.arcWidth = 66.0
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(frame: CGRect, arcWidth: CGFloat, startRadians: CGFloat, endRadians: CGFloat, buttonColor: UIColor) {
        self.arcWidth = arcWidth
        self.startRadians = startRadians
        self.endRadians = endRadians
        self.buttonColor = buttonColor
        super.init(frame: frame)
    }
    
    override func drawRect(rect: CGRect) {
        
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2)
        let radius: CGFloat = max(bounds.width, bounds.height)

        // Drawn on the unit circle.
        let startAngle: CGFloat =  startRadians * π
        let endAngle: CGFloat = endRadians * π
        
        let path = UIBezierPath(arcCenter: center,
            radius: radius/2 - arcWidth/2,
            startAngle: startAngle,
            endAngle: endAngle,
            clockwise: true)
        self.arc = path
        
        path.lineWidth = arcWidth
        if (activate) {
            let activeColor = UIColor.colorWithAlphaComponent(buttonColor)(0.2)
            activeColor.setStroke()
        } else {
            buttonColor.setStroke()
        }

        path.stroke()
    }

    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        
        let superResult = super.pointInside(point, withEvent: event)
        if (!superResult) {
            self.previousTouchResponse = superResult
            return superResult
        }
        
        // Return cached response in case we're testing the same point.
        if (CGPointEqualToPoint(point, self.previousTouchPoint)) {
            return self.previousTouchResponse
        } else {
            self.previousTouchPoint = point
        }
        
        let response = self.arc.containsPoint(point)
        self.previousTouchResponse = response
        
        return response;
    }

}
