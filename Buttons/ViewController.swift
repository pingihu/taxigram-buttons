//
//  ViewController.swift
//  Buttons
//
//  Created by Ping Inay Hu on 01.03.16.
//  Copyright © 2016 Ping Hu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // Only one button activated at a time.
    var buttons = [UIButton]()
    
    // Button group placement.
    var buttonX = CGFloat(100)
    var buttonY = CGFloat(100)

    // Size of entire button group and width of arc buttons.
    var buttonSize = CGFloat(200)
    var arcWidth = CGFloat(66)

    override func viewDidLoad() {
        super.viewDidLoad()

        let circDiameter = buttonSize - (2 * arcWidth)
        createArcButton("SVO", startArc: 7 / 6.0, endArc: 11 / 6.0, color: UIColor.greenColor(), titlePlacement: UIEdgeInsetsMake(0, 0, 135, 0))
        createArcButton("DME", startArc: 11 / 6.0, endArc: 1 / 2.0, color: UIColor.redColor(), titlePlacement: UIEdgeInsetsMake(60, 110, 0, 0))
        createArcButton("VKO", startArc: 1 / 2.0, endArc: 7 / 6.0, color: UIColor.blueColor(),
            titlePlacement: UIEdgeInsetsMake(60, 0, 0, 110))
        createCircButton("M", frame: CGRectMake(166, 166, circDiameter, circDiameter), color: UIColor.brownColor())
    }
    
    func createCircButton(title: String, frame: CGRect, color: UIColor) {
        let button = CircleButtonView();
        button.setTitle(title, forState: .Normal)
        button.setTitleColor(UIColor.blackColor(), forState: .Normal)
        button.buttonColor = color
        button.frame = frame
        button.addTarget(self, action: "buttonPressed:", forControlEvents: .TouchUpInside)
        self.view.addSubview(button)
        buttons.append(button)
    }
    
    func createArcButton(title: String, startArc: CGFloat, endArc: CGFloat, color: UIColor, titlePlacement: UIEdgeInsets) {
        
        // Look
        let buttonFrame = CGRectMake(buttonX, buttonY, buttonSize, buttonSize)
        let button = ArcButtonView(frame: buttonFrame, arcWidth: arcWidth, startRadians: startArc, endRadians: endArc, buttonColor: color)
        
        // Title
        button.setTitle(title, forState: .Normal)
        button.titleEdgeInsets = titlePlacement
        button.setTitleColor(UIColor.blackColor(), forState: .Normal)

        // Press functionality
        button.addTarget(self, action: "buttonPressed:", forControlEvents: .TouchUpInside)
        self.view.addSubview(button)
        buttons.append(button)
    }
    
    func buttonPressed(sender: UIButton!) {
        (sender as! ArcButtonView).activate = true
        let alertView = UIAlertView();
        alertView.addButtonWithTitle("Нет");
        alertView.title = "Внимание!";
        alertView.message = "Хотите добраться до \(sender.titleLabel!.text!)?";
        alertView.show();
        for button in buttons as! [ArcButtonView] {
            if (button.activate && button != sender) {
                button.activate = false
            }
        }
    }

}

