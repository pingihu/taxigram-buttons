//
//  CircleButtonView.swift
//  Buttons
//
//  Created by Ping Inay Hu on 02.03.16.
//  Copyright © 2016 Ping Hu. All rights reserved.
//

import UIKit

class CircleButtonView: ArcButtonView {

    override func drawRect(rect: CGRect) {
        let path = UIBezierPath(ovalInRect: rect)
        if (activate) {
            let activeColor = UIColor.colorWithAlphaComponent(buttonColor)(0.2)
            activeColor.setFill()
        } else {
            buttonColor.setFill()
        }

        path.fill()
        self.arc = path
    }

}
